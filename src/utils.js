import {document as doc, window as win} from 'ssr-window';

// This was taken from node_modules/framework7/js/framework7.js
export var Device = (function Device() {
    var platform = win.navigator.platform;
    var ua = win.navigator.userAgent;

    var device = {
      ios: false,
      android: false,
      androidChrome: false,
      desktop: false,
      windowsPhone: false,
      iphone: false,
      iphoneX: false,
      ipod: false,
      ipad: false,
      edge: false,
      ie: false,
      firefox: false,
      macos: false,
      windows: false,
      cordova: !!(win.cordova || win.phonegap),
      phonegap: !!(win.cordova || win.phonegap),
      electron: false,
    };

    var screenWidth = win.screen.width;
    var screenHeight = win.screen.height;

    var windowsPhone = ua.match(/(Windows Phone);?[\s\/]+([\d.]+)?/); // eslint-disable-line
    var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/); // eslint-disable-line
    var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
    var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
    var iphone = !ipad && ua.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
    var iphoneX = iphone && (
      (screenWidth === 375 && screenHeight === 812) // X/XS
      || (screenWidth === 414 && screenHeight === 896) // XR / XS Max
    );
    var ie = ua.indexOf('MSIE ') >= 0 || ua.indexOf('Trident/') >= 0;
    var edge = ua.indexOf('Edge/') >= 0;
    var firefox = ua.indexOf('Gecko/') >= 0 && ua.indexOf('Firefox/') >= 0;
    var macos = platform === 'MacIntel';
    var windows = platform === 'Win32';
    var electron = ua.toLowerCase().indexOf('electron') >= 0;

    device.ie = ie;
    device.edge = edge;
    device.firefox = firefox;

    // Windows
    if (windowsPhone) {
      device.os = 'windowsPhone';
      device.osVersion = windowsPhone[2];
      device.windowsPhone = true;
    }
    // Android
    if (android && !windows) {
      device.os = 'android';
      device.osVersion = android[2];
      device.android = true;
      device.androidChrome = ua.toLowerCase().indexOf('chrome') >= 0;
    }
    if (ipad || iphone || ipod) {
      device.os = 'ios';
      device.ios = true;
    }
    // iOS
    if (iphone && !ipod) {
      device.osVersion = iphone[2].replace(/_/g, '.');
      device.iphone = true;
      device.iphoneX = iphoneX;
    }
    if (ipad) {
      device.osVersion = ipad[2].replace(/_/g, '.');
      device.ipad = true;
    }
    if (ipod) {
      device.osVersion = ipod[3] ? ipod[3].replace(/_/g, '.') : null;
      device.ipod = true;
    }
    // iOS 8+ changed UA
    if (device.ios && device.osVersion && ua.indexOf('Version/') >= 0) {
      if (device.osVersion.split('.')[0] === '10') {
        device.osVersion = ua.toLowerCase().split('version/')[1].split(' ')[0];
      }
    }

    // Webview
    device.webView = !!((iphone || ipad || ipod) && (ua.match(/.*AppleWebKit(?!.*Safari)/i) || win.navigator.standalone))
      || (win.matchMedia && win.matchMedia('(display-mode: standalone)').matches);
    device.webview = device.webView;
    device.standalone = device.webView;

    // Desktop
    device.desktop = !(device.ios || device.android || device.windowsPhone) || electron;
    if (device.desktop) {
      device.electron = electron;
      device.macos = macos;
      device.windows = windows;
    }

    // Meta statusbar
    var metaStatusbar = doc.querySelector('meta[name="apple-mobile-web-app-status-bar-style"]');

    // Check for status bar and fullscreen app mode
    device.needsStatusbarOverlay = function needsStatusbarOverlay() {
      if (device.desktop) { return false; }
      if (device.standalone && device.ios && metaStatusbar && metaStatusbar.content === 'black-translucent') {
        return true;
      }
      if ((device.webView || (device.android && device.cordova)) && (win.innerWidth * win.innerHeight === win.screen.width * win.screen.height)) {
        if (device.iphoneX && (win.orientation === 90 || win.orientation === -90)) {
          return false;
        }
        return true;
      }
      return false;
    };
    device.statusbar = device.needsStatusbarOverlay();

    // Pixel Ratio
    device.pixelRatio = win.devicePixelRatio || 1;

    // Color Scheme
    var DARK = '(prefers-color-scheme: dark)';
    var LIGHT = '(prefers-color-scheme: light)';
    device.prefersColorScheme = function prefersColorTheme() {
      var theme;
      if (win.matchMedia && win.matchMedia(LIGHT).matches) {
        theme = 'light';
      }
      if (win.matchMedia && win.matchMedia(DARK).matches) {
        theme = 'dark';
      }
      return theme;
    };

    // Export object
    return device;
  }());


// This function adds css classes to <html> tag, according to detected device
// This code was made in Framework7. See node_modules/framework7/js/framework7.js
// See docs: https://framework7.io/docs/device.html
export function MakeDeviceCSS() {
    var classNames = [];
    var html = doc.querySelector('html');
    var metaStatusbar = doc.querySelector('meta[name="apple-mobile-web-app-status-bar-style"]');
    if (!html) { return; }
    if (Device.standalone && Device.ios && metaStatusbar && metaStatusbar.content === 'black-translucent') {
      classNames.push('device-full-viewport');
    }
  
    // Pixel Ratio
    classNames.push(("device-pixel-ratio-" + (Math.floor(Device.pixelRatio))));
    if (Device.pixelRatio >= 2) {
      classNames.push('device-retina');
    }
    // OS classes
    if (Device.os) {
      classNames.push(
        ("device-" + (Device.os)),
        ("device-" + (Device.os) + "-" + (Device.osVersion.split('.')[0])),
        ("device-" + (Device.os) + "-" + (Device.osVersion.replace(/\./g, '-')))
      );
      if (Device.os === 'ios') {
        var major = parseInt(Device.osVersion.split('.')[0], 10);
        for (var i = major - 1; i >= 6; i -= 1) {
        classNames.push(("device-ios-gt-" + i));
        }
        if (Device.iphoneX) {
        classNames.push('device-iphone-x');
        }
      }
    } else if (Device.desktop) {
      classNames.push('device-desktop');
      if (Device.macos) { classNames.push('device-macos'); }
      else if (Device.windows) { classNames.push('device-windows'); }
    }
    if (Device.cordova || Device.phonegap) {
      classNames.push('device-cordova');
    }
  
    // Add html classes
    classNames.forEach(function (className) {
      html.classList.add(className);
    });
  }

  import { main_popup } from './store.js';
  export function show_img_in_popup(a_node) {
    let src = a_node.href;
    let html = '<img src="' + src + '" alt="" width="100%" />'
    let page = doc.querySelector('.page-content');
    let position = page.scrollHeight - page.clientHeight
    console.log(page.scrollHeight);
    console.log(page.clientHeight);
    main_popup.update(obj => { return {
      'content': html,
      'show': true,
      'y': -page.clientHeight + 400
    }});
  }
  // TODO 
  // Поправить позициоонирование Popup на Desktop и на мобильных.