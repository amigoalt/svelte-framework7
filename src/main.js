import App from "./App.svelte";

new App({
  target: document.getElementById("svelte_app"),
  hydrate: true
});
