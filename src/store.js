import { writable } from 'svelte/store';
import Home from "./routes/Home.svelte";
import About from "./routes/About.svelte";
import Blog from "./routes/Blog.svelte";


/* === SIDEBAR === */
export const sidebar = writable({
    visible: false,
    items: [
        {'path': 'home', 'title': 'Главная', 'comp': Home, 'slide': 'assets/img/slide_about.jpg'},
        {'path': 'about', 'title': 'Об адвокате', 'comp': About, 'slide': 'assets/img/slide_about.jpg'},
        {'path': 'services', 'title': 'Услуги', 'comp': Home, 'slide': 'assets/img/slide_about.jpg'},
        {'path': 'practice', 'title': 'Практика', 'comp': Home, 'slide': 'assets/img/slide_about.jpg'},
        {'path': 'blog', 'title': 'Блог', 'comp': Blog, 'slide': 'assets/img/slide_about.jpg'},
        {'path': 'contacts', 'title': 'Контакты', 'comp': Blog, 'slide': 'assets/img/slide_about.jpg'}
    ]
})


/* === Main popup === */
export const main_popup = writable({
    show: false,
    content: 'default popup content',
    y: 0
})