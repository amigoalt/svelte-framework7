import svelte from "rollup-plugin-svelte";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import livereload from "rollup-plugin-livereload";
import { terser } from "rollup-plugin-terser";
// import ignore from 'rollup-plugin-ignore';
import autoPreprocess from 'svelte-preprocess'


/*
import path from 'path';
const f7_accordion = path.resolve( __dirname, 'node_modules/framework7/components/accordion/accordion.js' );
const framework7 = path.resolve( __dirname, 'node_modules/framework7/framework7.esm.bundle.js' );
*/

const isDev = Boolean(process.env.ROLLUP_WATCH);

export default [
  // Browser bundle
  {
    input: "src/main.js",
    output: {
      sourcemap: true,
      format: "iife",
      name: "app",
      file: "public/bundle.js",
    },
    plugins: [
      svelte({
        hydratable: true,
        preprocess: autoPreprocess({
          transformers: {
            postcss: {
              plugins: [require('postcss-import')],
            },
          },
        }),
        css: css => {
          css.write("public/bundle.css");
        }
      }),
      resolve(),
      commonjs(),
      // App.js will be built after bundle.js, so we only need to watch that.
      // By setting a small delay the Node server has a chance to restart before reloading.
      isDev &&
        livereload({
          watch: "public/App.js",
          delay: 2000
        }),
      !isDev && terser()
    ]
  },
  // Server bundle
  {
    input: "src/App.svelte",
    output: {
      sourcemap: false,
      format: "cjs",
      name: "app",
      file: "public/App.js",
    },
    plugins: [
      /*
      ignore([
        '/home/good/python_projects/svelte-routing/node_modules/framework7/framework7.esm.bundle.js',
        '/home/good/python_projects/svelte-routing/node_modules/framework7/components/accordion/accordion.js'
      ]),
      */
      svelte({
        generate: "ssr"
      }),
      resolve(),
      commonjs(),
      !isDev && terser()
    ]
  }
];
