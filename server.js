const path = require("path");
const express = require("express");
const app = require("./public/App.js");

const server = express();

server.use(express.static(path.join(__dirname, "public")));

server.get("*", function(req, res) {
  const { html } = app.render({ url: req.url });

  res.write(`
  <html class="md">
  <head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Color theme for statusbar -->
    <meta name="theme-color" content="#2196f3">
    <!-- Your app title -->
    <title>My App</title>
    <!-- Path to your custom app styles-->
    <link rel='stylesheet' href='/global.css'>
    <link rel="stylesheet" href="/assets/framework7/css/framework7.bundle.min.css">
    <link rel="stylesheet" href="/assets/framework7/css/icons.css">

    <link rel='stylesheet' href='/bundle.css'>
  </head>
  <body>
    <div id="svelte_app" class="framework7-root">
      ${html}
    </div>
    <!-- Path to your app js-->
    <script type="text/javascript" src="/bundle.js"></script>
  </body>
</html>

  `);

  res.end();
});

const port = 3000;
server.listen(port, () => console.log(`Listening on port ${port}`));
