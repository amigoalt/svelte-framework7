# SvelteJS-Framework7

1. SvelteJS UI components based on Framework7 HTML/CSS templates.
2. Svelte-Routing (thanks to [EmilTholin's Svelte-Routing](https://github.com/EmilTholin/svelte-routing))

## Basic usage

To make using of Framework7 CSS you should do following:

- **npm install --save-dev svelte-preprocess postcss postcss-import**
- **edit rollup.config.js**


```
import autoPreprocess from 'svelte-preprocess'
...
svelte({
    preprocess: autoPreprocess({
        transformers: {
            postcss: {
                plugins: [require('postcss-import')]
            }
        }
    })
})
```
- **In the Svelte component add syle import:**
```
<style>
    @import url('../../node_modules/framework7/components/panel.css');
</style>
```
- **Also, dont forget to link Framework7 core css in the <head> section of your page.**